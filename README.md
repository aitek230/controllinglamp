# ControllingLamp
## Description
This is application for IoT Controlling for Lamp, you must be join to Telkom IoT Workshop for know how to implement and integration this app.

## Detail Tools
This apps is building with 
* build gradle 3.4.1
* compileSdkVersion 29
* buildToolsVersion 29.0.0
* minSdkVersion 19
* targetSdkVersion 29

This apps is building with library
* androidx.appcompat:appcompat:1.1.0
* androidx.constraintlayout:constraintlayout:1.1.3
* androidx.core:core:1.2.0-rc01
* com.android.volley:volley:1.1.1

## Copywrite
You can using this as open source, but don't forget to add comment in the issue to get permission.
@copywrite Telkom IoT Center.